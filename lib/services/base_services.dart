
import '../model/login_master.dart';
import '../model/movie_list_data.dart';
import '../model/user_list_master.dart';

abstract class BaseServices {
  Future<LoginMaster?> login({required body, onNoInternet, onError});

  Future<UserListMaster?> userList({pageNumber, onNoInternet, onError});

  Future<MoviesListData?> getPopularMovies();
}
