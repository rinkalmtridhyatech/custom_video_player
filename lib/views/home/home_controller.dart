import 'package:tiktok/utils/app_constants.dart';
import 'package:video_player/video_player.dart';

import '../../controller/base_controller.dart';
import '../../utils/utility.dart';

class HomeController extends BaseController {
  var data = AppConstants.videoData;
  VideoPlayerController? videoPlayController;

  int prevVideo = 0;

  int actualScreen = 0;

  @override
  void onInit() {
    loadVideo(0);
    loadVideo(1);
    super.onInit();
  }

  changeVideo(index) async {
    if (data[index].controller == null) {
      await data[index].loadController();
    }
    data[index].controller!.play();

    if (data[index].controller != null) {
      data[prevVideo].controller!.pause();
    }

    prevVideo = index;
    update();

    printf(index);
  }

  void loadVideo(int index) async {
    if (data.length > index) {
      await data[index].loadController();
      data[index].controller?.play();
      update();
    }
  }

  @override
  void dispose() {
    videoPlayController?.dispose();
    super.dispose();
  }
}
