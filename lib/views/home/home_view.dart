import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_fade/image_fade.dart';
import 'package:tiktok/utils/app_constants.dart';
import 'package:tiktok/views/home/home_controller.dart';
import 'package:video_player/video_player.dart';

import '../../model/video_data.dart';

class HomeView extends GetView<HomeController> {
  static const double navigationIconSize = 20.0;
  static const double actionWidgetSize = 60.0;
  static const double actionIconSize = 35.0;
  static const double shareActionIconSize = 25.0;
  static const double profileImageSize = 50.0;
  static const double actionIconGap = 12.0;
  static const double followActionIconSize = 25.0;
  static const double createButtonWidth = 38.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<HomeController>(builder: (context) {
        return PageView.builder(
          controller: PageController(
            initialPage: 0,
            viewportFraction: 1,
          ),
          itemCount: controller.data.length,
          onPageChanged: (index) {
            index = index % (controller.data.length);
            controller.changeVideo(index);
            controller.update();
          },
          scrollDirection: Axis.vertical,
          itemBuilder: (context, index) {
            index = index % (controller.data.length);
            return videoCard(controller.data[index]);
          },
        );
      }),
    );
  }

  Widget videoCard(VideoData video) {
    return Stack(
      children: [
        video.controller != null
            ? GestureDetector(
                onTap: () {
                  if (video.controller!.value.isPlaying) {
                    video.controller?.pause();
                  } else {
                    video.controller?.play();
                  }
                },
                child: SizedBox.expand(
                    child: FittedBox(
                  fit: BoxFit.cover,
                  child: SizedBox(
                    width: video.controller?.value.size.width ?? 0,
                    height: video.controller?.value.size.height ?? 0,
                    child: VideoPlayer(video.controller!),
                  ),
                )),
              )
            : Container(
                color: Colors.black,
                child: const Center(
                  child: Text(AppConstants.loading),
                ),
              ),
        Column(
          children: <Widget>[
            followingContainer,
            centerSection(video),
            Opacity(
                opacity: 0.1,
                child: Container(height: 1.0, color: Colors.grey[300])),
          ],
        ),
      ],
    );
  }

  Widget get followingContainer => Container(
        height: 100.0,
        padding: const EdgeInsets.only(bottom: 15.0),
        alignment: const Alignment(0.0, 1.0),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const Text(AppConstants.following),
              Container(
                width: 15.0,
              ),
              const Text(AppConstants.following,
                  style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold))
            ]),
      );

  Widget videoDescription(VideoData data) {
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.only(left: 20.0),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              data.user,
              style: const TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white),
            ),
            Container(
              height: 10.0,
            ),
            Text(
              data.videoTitle,
              style: const TextStyle(color: Colors.white),
            ),
            Container(
              height: 10.0,
            ),
            Row(children: [
              const Icon(Icons.music_note, color: Colors.white, size: 15.0),
              Container(
                width: 10.0,
              ),
              const Text(AppConstants.singerName,
                  style: TextStyle(fontSize: 12.0, color: Colors.white)),
              Container(
                width: 10.0,
              ),
              const Text(AppConstants.songName,
                  style: TextStyle(fontSize: 12.0, color: Colors.white))
            ]),
            Container(
              height: 12.0,
            ),
          ]),
    ));
  }

  Widget actionsToolbar(VideoData data) {
    return SizedBox(
      width: 100.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _getProfileVideoAction(data.userPic),
          _getVideoAction(
              title: '${data.likes}m',
              icon: const IconData(0xf737, fontFamily: 'MaterialIcons')),
          _getVideoAction(
              title: '${data.comments}k',
              icon: const IconData(0xe154, fontFamily: 'MaterialIcons')),
          _getVideoAction(
              title: AppConstants.share,
              icon: const IconData(0xf00fe, fontFamily: 'MaterialIcons'),
              isShare: true),
          _getMusicPlayerAction()
        ],
      ),
    );
  }

  LinearGradient get musicGradient => const LinearGradient(colors: [
        Colors.black54,
        Colors.black87,
        Colors.black54,
        Colors.black87,
      ], stops: [
        0.0,
        0.4,
        0.6,
        1.0
      ], begin: Alignment.bottomLeft, end: Alignment.topRight);

  Widget _getMusicPlayerAction() {
    return Container(
        margin: const EdgeInsets.only(top: 10.0),
        width: actionWidgetSize,
        height: actionWidgetSize,
        child: Column(children: [
          Container(
            padding: const EdgeInsets.all(11.0),
            height: profileImageSize,
            width: profileImageSize,
            decoration: BoxDecoration(
                gradient: musicGradient,
                borderRadius: BorderRadius.circular(profileImageSize / 2)),
            child: const ImageFade(
              image: NetworkImage(
                  "https://images.saymedia-content.com/.image/ar_16:9%2Cc_fill%2Ccs_srgb%2Cq_auto:eco%2Cw_1200/MTk4MDQzMTI5NzY3NTM1ODA2/short-captions-for-profile-pictures.png"),
            ),
          ),
        ]));
  }

  Widget centerSection(VideoData data) {
    return Expanded(
        child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[videoDescription(data), actionsToolbar(data)]));
  }

  Widget _getVideoAction(
      {String? title, IconData? icon, bool isShare = false}) {
    return Container(
        margin: const EdgeInsets.only(top: 15.0),
        width: actionWidgetSize,
        height: actionWidgetSize,
        child: Column(children: [
          Icon(icon,
              size: isShare ? shareActionIconSize : actionIconSize,
              color: Colors.grey[300]),
          Padding(
            padding: EdgeInsets.only(top: isShare ? 5.0 : 2.0),
            child: Text(title!,
                style: TextStyle(
                    fontSize: isShare ? 10.0 : 12.0, color: Colors.white)),
          )
        ]));
  }

  Widget _getProfileVideoAction(String pictureUrl) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(top: 10.0),
          width: actionWidgetSize,
          height: actionWidgetSize,
          child: Column(children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(30.0),
              child: Container(
                padding: const EdgeInsets.all(1.0),
                height: profileImageSize,
                width: profileImageSize,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(profileImageSize / 2)),
                child: ImageFade(
                  fit: BoxFit.cover,
                  image: NetworkImage(pictureUrl),
                ),
              ),
            ),
          ])),
      Positioned(
          width: 15.0,
          height: 15.0,
          bottom: 5,
          left: ((actionWidgetSize / 2) - (15 / 2)),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(15.0)),
          )),
      const Positioned(
          bottom: 0,
          left: ((actionWidgetSize / 2) - (followActionIconSize / 2)),
          child: Icon(Icons.add_circle,
              color: Color.fromARGB(255, 255, 43, 84),
              size: followActionIconSize))
    ]);
  }
}
