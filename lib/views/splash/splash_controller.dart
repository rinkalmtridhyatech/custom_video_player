import 'dart:async';

import 'package:get/get.dart';

import '../../controller/base_controller.dart';
import '../../database/app_preferences.dart';
import '../../routes/app_pages.dart';
import '../../style/dimensions.dart';


class SplashController extends BaseController {
  AppPreferences appPreferences = AppPreferences();

  @override
  void onInit() {
    splashTimer();
    super.onInit();
  }

  void splashTimer() async {
    var duration = Duration(
      seconds: Dimensions.screenLoadTime,
    );
    Future.delayed(duration, () async {
      Get.offNamedUntil(Routes.DASHBOARD, (route) => false);
    });
  }
}
