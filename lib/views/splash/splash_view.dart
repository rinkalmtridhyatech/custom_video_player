import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:tiktok/utils/image_paths.dart';
import 'package:tiktok/utils/svg_extension.dart';
import 'package:tiktok/views/splash/splash_controller.dart';

import '../../style/color_constants.dart';
import '../../style/dimensions.dart';

class SplashView extends GetView<SplashController> {
  SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return GetBuilder(
      init: SplashController(),
      builder: (SplashController controller) {
        return Scaffold(
          body: Container(
            color: ColorConstants.white,
            height: Dimensions.screenHeight,
            width: Dimensions.screenWidth,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                    height: 180,
                    width: 180,
                    child: ImagePath.logoTikTok.toSvg)
              ],
            ),
          ),
        );
      },
    );
  }
}
