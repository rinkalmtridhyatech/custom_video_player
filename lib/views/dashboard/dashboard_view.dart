import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/bottom_toolbar.dart';
import 'dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  const DashboardView({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: BottomToolbar(
          selectedIndex: controller.selectedIndex.value,
          callback: (index) {
            controller.selectedIndex.value = index;
          },
        ),
        body:
            controller.widgetOptions.elementAt(controller.selectedIndex.value),
      ),
    );
  }
}
