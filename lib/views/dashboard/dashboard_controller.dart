import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tiktok/views/home/home_controller.dart';

import '../../controller/base_controller.dart';
import '../home/home_view.dart';
import '../movie/movie_controller.dart';
import '../movie/movie_list_view.dart';

class DashboardController extends BaseController {
  final MovieController controller = Get.put(MovieController());
  final HomeController homeController = Get.put(HomeController());
  var selectedIndex = 0.obs;

  List<Widget> widgetOptions = <Widget>[
    HomeView(),
    MovieListView(),
    MovieListView(),
    MovieListView(),
    MovieListView()
  ];
}
