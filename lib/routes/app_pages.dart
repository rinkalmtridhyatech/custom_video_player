import 'package:get/get.dart';

import '../views/dashboard/dashboard_binding.dart';
import '../views/dashboard/dashboard_view.dart';
import '../views/splash/splash_binding.dart';
import '../views/splash/splash_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
        name: Routes.SPLASH,
        page: () => SplashView(),
        binding: SplashBinding(),
        transition: Transition.downToUp),
    GetPage(
        name: Routes.DASHBOARD,
        page: () => DashboardView(),
        binding: DashboardBinding(),
        transition: Transition.rightToLeft),
  ];
}
