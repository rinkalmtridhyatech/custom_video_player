import '../model/video_data.dart';

class AppConstants {
  //App related
  static const String appName = 'Setup App';
  static const int appVersion = 1;

  //Languages
  static const String languageEnglish = "en";
  static const String languageGerman = "de";

  //API related
  static const String baseApi = 'http://restapi.adequateshop.com/api';
  static const String login = '$baseApi/authaccount/login';
  static const String userList = '$baseApi/users';
  static const String movieDbBaseUrl = 'https://api.themoviedb.org/3';
  static const String movieDbImageBaseUrl = 'https://image.tmdb.org/t/p/w500';
  static const String discoverMoviesUrl =
      '/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc';
  static const String movieDbApiKey = 'a85ce05af29f9b75de6860905c3fde99';
  static const String movieDbBearerToken =
      'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhODVjZTA1YWYyOWY5Yjc1ZGU2ODYwOTA1YzNmZGU5OSIsInN1YiI6IjY1MDdlYTIxM2NkMTJjMDEwY2QwZmQxMSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.t3NiqjWxCiqbS9JfJRRpzu7jAmHGAV23QrY62iSspW8';

  //API results
  static const String checkInFail = 'Check-In Failed';

  /*
Shared Pref
  */
  static const String apiKey = 'apiKey';

  //BottomTab
  static const String home = 'Home';
  static const String search = 'Search';
  static const String inbox = 'Inbox';
  static const String account = 'Account';
  static const String me = 'Me';

  //dummy data
  static var videoData = [
    VideoData(
        id: "1",
        url:
        "https://firebasestorage.googleapis.com/v0/b/videostreaming-test.appspot.com/o/vid%2FSnaptik_6745671851688692998_tiktok.mp4?alt=media&token=e6c76be2-9d8e-4be6-aedc-89ddd4985871",
        videoTitle: "test 1",
        songName: "Song 1 - Artist 1",
        likes: "2.6",
        comments: "143",
        userPic:
        "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg",
        user: 'User 1'),
    VideoData(
        id: "2",
        userPic:
        "https://i.pinimg.com/originals/5e/eb/8d/5eeb8d615bea040425f9937699392751.jpg",
        videoTitle: "test 2",
        songName: "Song 2 - Artist 2",
        likes: "2",
        comments: "67",
        url:
        "https://firebasestorage.googleapis.com/v0/b/videostreaming-test.appspot.com/o/vid%2FSnaptik_6842407707551599878_carlos-barrios%20(1).mp4?alt=media&token=965f5080-2771-4477-bd9d-defc7b581c5d",
        user: 'User 2'),
    VideoData(
        id: "3",
        userPic:
        "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg",
        videoTitle: "test 3",
        songName: "Song 3 - Artist 3",
        likes: "1.1",
        comments: "973",
        url:
        "https://firebasestorage.googleapis.com/v0/b/videostreaming-test.appspot.com/o/vid%2FSnaptik_6856769842385620229_alex.mp4?alt=media&token=b70d853b-760a-45ee-b5d3-44cef7e4db7f",
        user: 'User 3'),
  ];

  //Home
  static const String following =  'Following';
  static const String forYou =  'For you';
  static const String loading =  'Loading';
  static const String singerName =  'Singer name';
  static const String songName =  'Song name';
  static const String share =  'Share';

  //Movie
  static const String discoverMoviesTxt = 'Discover Movies';
}
