import 'package:flutter/material.dart';
import 'package:tiktok/utils/app_constants.dart';

class BottomToolbar extends StatelessWidget {
  static const double navigationIconSize = 20.0;
  static const double createButtonWidth = 34.0;
  int selectedIndex = 0;
  late final void Function(int) callback;

  BottomToolbar(
      {super.key, required this.selectedIndex, required this.callback});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.black,
      currentIndex: selectedIndex,
      items: [
        item(Icons.home, AppConstants.home, false),
        item(Icons.search, AppConstants.search, false),
        item(Icons.add, AppConstants.search, true),
        item(Icons.messenger, AppConstants.inbox, false),
        item(Icons.person_outlined, AppConstants.me, false),
      ],
      onTap: (index) {
        callback(index);
      },
    );
  }

  BottomNavigationBarItem item(IconData icon, String title, bool isCustom) {
    return BottomNavigationBarItem(
      backgroundColor: Colors.black,
      icon: isCustom ? customIcon() : Icon(icon, size: 25),
      label: isCustom ? '' : title,
    );
  }

  Widget customIcon() {
    return SizedBox(
      width: 45,
      height: 30,
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(
              left: 10,
            ),
            width: 38,
            decoration: BoxDecoration(
              color: const Color.fromARGB(
                255,
                250,
                45,
                108,
              ),
              borderRadius: BorderRadius.circular(7),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              right: 10,
            ),
            width: 38,
            decoration: BoxDecoration(
              color: const Color.fromARGB(
                255,
                32,
                211,
                234,
              ),
              borderRadius: BorderRadius.circular(7),
            ),
          ),
          Center(
            child: Container(
              height: double.infinity,
              width: 38,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(7),
              ),
              child: const Icon(
                Icons.add,
                color: Colors.black,
                size: 20,
              ),
            ),
          )
        ],
      ),
    );
  }
}
