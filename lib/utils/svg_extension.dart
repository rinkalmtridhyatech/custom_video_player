import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

extension StringExtension on String? {

  /// Extension on String which returns SvgPicture Widget with a default width
  /// Write asset_name & use .svg() method to get SvgPicture Widget.
  Widget svg({double width = 26}) {
    String path = '$this';
    return SvgPicture.asset(
      path,
      // width: width,
    );
  }


  /// [toSvg] getter to easily access the extension method
  Widget get toSvg => svg();
}
