import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/login_master.dart';
import '../utils/utility.dart';

class AppPreferences {
  static final AppPreferences _instance = AppPreferences._internal();

  factory AppPreferences() => _instance;

  AppPreferences._internal();

  //------------------------- Preference Constants -----------------------------
  static const String keyLanguageCode = "keyLanguageCode";
  static const String keyLoginDetails = "keyLoginDetails";

  // Method to get language code
  Future<String?> getLanguageCode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(keyLanguageCode);
  }

  // Method to set language code
  Future<bool> setLanguageCode(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(keyLanguageCode, value);
  }

  // Method to get login details
  Future<LoginDetails?> getLoginDetails() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? loginDetails = prefs.getString(keyLoginDetails);
    if (Utility.isEmpty(loginDetails)) {
      return null;
    }
    try {
      return LoginDetails.fromJson(json.decode(loginDetails!));
    } catch (e) {
      printf("App Preference error: ${e.toString()}");
    }
    return null;
  }

  // Method to set login details
  Future<bool> setLoginDetails(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(keyLoginDetails, value);
  }
}
